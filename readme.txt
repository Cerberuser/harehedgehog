\HazeIgel
  The main project. A .NET Core 2.1 Web API application. Contains all the logic.
  Can be deployed under Windows / Linux OS or started from MS Visual Studio under Kestrel web server.

\HazeIgelTest
  The test project, uses NUnit NuGet. Also contains a simple game client (\HazeIgelTest\front), 
  that is made of static html/css/js. Requires a running server (\HazeIgel), server's address 
  is provided in the game.js file.

  This web app is made too simple intentionally to avoid possible conflicts 
  with "Hase und Igel" copyright holders.

=========================================
HazeIgel project
=========================================
The main files are:

- \HazeIgel\Model\FieldMap.cs - in essence, the array of cells. A game map, that
  remains untouched during the whole game

- \HazeIgel\Model\Disposition.cs - describes the current game "snapshot":
  players' attributes and the current player index

- \HazeIgel\Model\Haze.cs - player with his attributes

- \HazeIgel\Model\TurnChecker.cs - a class that:
    - checks if the provided move is allowed
    - makes the provided move
    - gives all the possible state for the provided game "snapshot"

- \HazeIgel\Model\AI\EstimateCalculator.cs - a class that used heuristic to estimate how
  "good" (or how "bad") the provided player's position is

- \HazeIgel\Model\AI\TurnMaker.cs - a class that solves the Minimax game tree and chooses
  the "best" move

- \HazeIgel\Model\Game\ - a set of classes used to implement REST API

- \HazeIgel\Controllers\GameController.cs - a WEB API controller, implements just one
  POST request for one of the \HazeIgel\Model\Game\GameCommandxxxx.cs commands