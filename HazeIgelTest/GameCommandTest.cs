﻿using HazeIgel.Model;
using HazeIgel.Model.Game;
using Newtonsoft.Json;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class GameCommandTest
    {
        [Test]
        public void TestNewGameCommand()
        {
            var cmd = new GameCommandNewGame
            {
                aiPlayers = 2,
                humanPlayers = 3
            };
            var parsed = GameCommand.ParseCommand(JsonConvert.SerializeObject(cmd)) as GameCommandNewGame;
            Assert.IsNotNull(parsed);
            Assert.AreEqual(cmd.aiPlayers, parsed.aiPlayers);
            Assert.AreEqual(cmd.humanPlayers, parsed.humanPlayers);
        }

        [Test]
        public void TestPlayerTurnCommand()
        {
            var cmd = new GameCommandPlayerTurn
            {
                command = 3,
                disp = new Disposition
                {
                    hazes = new []
                    {
                        new Haze(0)
                        {
                            pos = 11,
                            carrots = 23,
                            salad = 2,
                            waits = false
                        },
                        new Haze(1)
                        {
                            pos = 26,
                            carrots = 1,
                            salad = 0,
                            waits = true
                        }
                    },
                    activeHaze = 1
                }
            };
            var json = JsonConvert.SerializeObject(cmd);
            var parsed = GameCommand.ParseCommand(json) as GameCommandPlayerTurn;
            Assert.IsNotNull(parsed);
            Assert.AreEqual(cmd.command, parsed.command);
            Assert.AreEqual(cmd.disp.activeHaze, parsed.disp.activeHaze);
            Assert.AreEqual(cmd.disp.hazes[0].carrots, parsed.disp.hazes[0].carrots);
            Assert.AreEqual(cmd.disp.hazes[1].waits, parsed.disp.hazes[1].waits);
        }

        [Test]
        public void TestAiTurnCommand()
        {
            var cmd = new GameCommandAiTurn();
            var parsed = GameCommand.ParseCommand(JsonConvert.SerializeObject(cmd)) as GameCommandAiTurn;
            Assert.IsNotNull(parsed);
        }
    }
}
