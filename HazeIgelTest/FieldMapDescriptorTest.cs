﻿using HazeIgel.Model;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class FieldMapDescriptorTest
    {
        [Test]
        public void TestIgelPos()
        {
            var map = FieldMap.Instance;
            var des = new FieldMapDescriptor(map);
            Assert.AreEqual(50, des.nearestIgelByPos[52]);
            Assert.AreEqual(43, des.nearestIgelByPos[50]);
            Assert.AreEqual(-1, des.nearestIgelByPos[8]);
        }
    }
}
