﻿using System.Linq;
using HazeIgel.Model;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class PossibleMovesTest : TurnCheckerBaseTest
    {
        [Test]
        public void TestMoveForwardOnly()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(12, 22, 2, false),
                SpawnHaze(11, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.AreEqual(5, turns.Count);
        }

        [Test]
        public void TestLeapBack()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(12, 0, 2, false),
                SpawnHaze(11, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.AreEqual(1, turns.Count);
            Assert.AreEqual(0, turns[0]);

            disp = MakeDisposition(0,
                SpawnHaze(21, 0, 0, false),
                SpawnHaze(10, 4, 2, false));
            checker = new TurnChecker(map, disp);
            turns = checker.GetPossibleTurns();
            Assert.AreEqual(2, turns.Count);

            disp = MakeDisposition(0,
                SpawnHaze(12, 7, 2, false),
                SpawnHaze(10, 4, 2, false));
            checker = new TurnChecker(map, disp);
            turns = checker.GetPossibleTurns();
            Assert.AreEqual(3, turns.Count);
        }

        [Test]
        public void TestCanFinish()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(60, 2, 0, false),
                SpawnHaze(64, 4, 0, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.AreEqual(4, turns.Count);
            Assert.IsTrue(HasExactSameItems(turns, new []{ 1, 3, 4, -4}));

            disp = MakeDisposition(0,
                SpawnHaze(60, 12, 0, false),
                SpawnHaze(64, 4, 0, false));
            checker = new TurnChecker(map, disp);
            turns = checker.GetPossibleTurns();
            Assert.AreEqual(3, turns.Count);
        }

        [Test]
        public void TestExplainedSteps()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(60, 2, 0, false),
                SpawnHaze(64, 4, 0, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetExplainedSteps();

            Assert.AreEqual(4, turns.Count);
            turns = turns.OrderBy(t => t.command).ToList();
            Assert.IsTrue(turns.Last().finished);
            Assert.IsFalse(turns[2].finished);
        }

        [Test]
        public void TestCanWaitOnSalad()
        {
            var disp = MakeDisposition(1,
                SpawnHaze(60, 4, 0, false),
                SpawnHaze(57, 3, 1, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.IsTrue(HasExactSameItems(turns, new[] { 1, 2, 4, 5, 6, -1 }));
        }

        [Test]
        public void TestCanStepOnSaladUnloaded()
        {
            var disp = MakeDisposition(1,
                SpawnHaze(60, 4, 0, false),
                SpawnHaze(55, 10, 0, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.IsFalse(turns.Contains(0));
        }

        [Test]
        public void TestForcedWaitOnSaladUnloaded()
        {
            var disp = MakeDisposition(2,
                SpawnHaze(63, 65, 0, false),
                SpawnHaze(56, 21, 0, false),
                SpawnHaze(62, 0, 0, false));
            var checker = new TurnChecker(map, disp);
            var turns = checker.GetPossibleTurns();
            Assert.IsTrue(HasExactSameItems(turns, new [] { 0 }));
            var newDisp = checker.CheckTurnAndUpdate(0);
            Assert.AreEqual(0, newDisp.hazes[2].salad);
        }
    }
}
