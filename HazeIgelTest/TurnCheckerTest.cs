using System;
using HazeIgel.Model;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class TurnCheckerTest : TurnCheckerBaseTest
    {
        [Test]
        public void TestWaiting()
        {
            var otherHaze = SpawnHaze(7, 13, 2, false);
            var disp = MakeDisposition(0,
                SpawnHaze(4, 21, 2, false),
                otherHaze);

            // ... but can wait on finish
            disp.ActiveHaze.pos = map.fieldsCount - 1;
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(0);
            Assert.NotNull(newDisp);
            Assert.AreEqual(map.fieldsCount - 1, newDisp.hazes[0].pos);
            Assert.AreEqual(1, newDisp.activeHaze);

            // and on wait stage
            disp.ActiveHaze.pos = 21;
            disp.ActiveHaze.waits = true;
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(0);
            Assert.NotNull(newDisp);
            Assert.AreEqual(21, newDisp.hazes[0].pos);
            Assert.IsFalse(newDisp.hazes[0].waits);

            // ... on salad
            var oldSalad = disp.ActiveHaze.salad;
            disp.ActiveHaze.pos = Array.FindIndex(map.cells, c => c is SaladCell);
            disp.ActiveHaze.waits = true;
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(0);
            Assert.NotNull(newDisp);
            Assert.IsFalse(newDisp.hazes[0].waits);
            Assert.AreEqual(oldSalad - 1, newDisp.hazes[0].salad);

            // wait if has nowhere to move
            disp.ActiveHaze.carrots = 0;
            disp.ActiveHaze.pos = 20;
            disp.ActiveHaze.waits = false;
            otherHaze.pos = 19;
            disp.UpdatePositions(map);
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(0);
            Assert.NotNull(newDisp);
            Assert.AreEqual(20, newDisp.hazes[0].pos);
            Assert.IsFalse(newDisp.hazes[0].waits);
        }

        [Test]
        public void TestWhenNowhereToMove()
        {
            var disp = MakeDisposition(1,
                SpawnHaze(15, 13, 3, false),
                SpawnHaze(18, 2, 3, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(0);
            Assert.NotNull(newDisp);
        }

        [Test]
        public void TestWaitingFail()
        {
            // can't wait while is not in waiting state
            var disp = MakeDisposition(0,
                SpawnHaze(4, 21, 2, false),
                SpawnHaze(7, 13, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(0);
            Assert.IsNull(newDisp);

            // can't wait if broke, but on carrot
            disp = MakeDisposition(0,
                SpawnHaze(21, 0, 2, false),
                SpawnHaze(19, 13, 2, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(0);
            Assert.IsNull(newDisp);

            // can't wait if broke, but is leaving salad
            disp = MakeDisposition(0,
                SpawnHaze(22, 0, 2, false),
                SpawnHaze(19, 13, 2, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(0);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestLeapBackOccupied()
        {
            // can't leap back cause the cell is occupied
            var disp = MakeDisposition(0,
                SpawnHaze(29, 31, 2, false),
                SpawnHaze(24, 12, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(24 - 31);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestLeapBackNotOnIgel()
        {
            // can't leap back cause the target is not igel
            var disp = MakeDisposition(0,
                SpawnHaze(29, 36, 2, false),
                SpawnHaze(7, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(24 - 31 + 1);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestLeapBackWaiting()
        {
            // can't leap back cause the hase is waiting
            var disp = MakeDisposition(0,
                SpawnHaze(29, 36, 2, true),
                SpawnHaze(7, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(24 - 31);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestLeapBackOk()
        {
            var oldCarrots = 36;
            var disp = MakeDisposition(0,
                SpawnHaze(29, oldCarrots, 2, false),
                SpawnHaze(7, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(24 - 29);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(oldCarrots + (29 - 24) * 10, newDisp.hazes[0].carrots);
            Assert.AreEqual(24, newDisp.hazes[0].pos);
        }

        [Test]
        public void TestMoveOccupied()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(10, 14, 2, false),
                SpawnHaze(12, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(2);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestMoveFewCarrots()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(4, 9, 2, false),
                SpawnHaze(12, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(6);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestMoveOnSaladWoSalad()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(5, 9, 0, false),
                SpawnHaze(12, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(2);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestMoveOnSaladOk()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(5, 9, 1, false),
                SpawnHaze(12, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(2);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(7, newDisp.hazes[0].pos);
            Assert.AreEqual(9 - 3, newDisp.hazes[0].carrots);
            Assert.AreEqual(1, newDisp.hazes[0].salad);
            Assert.IsTrue(newDisp.hazes[0].waits);
        }

        [Test]
        public void TestMoveOk()
        {
            var oldCarrots = 10;
            var disp = MakeDisposition(0,
                SpawnHaze(10, oldCarrots, 2, false),
                SpawnHaze(11, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(6);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(10 + 20 - 21, newDisp.hazes[0].carrots);
            Assert.AreEqual(16, newDisp.hazes[0].pos);
        }

        [Test]
        public void TestFinish()
        {
            // to much carrots
            var disp = MakeDisposition(0,
                SpawnHaze(61, 21 + 6, 0, false),
                SpawnHaze(64, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(3);
            Assert.IsNull(newDisp);

            // to much salad
            disp = MakeDisposition(0,
                SpawnHaze(61, 18, 1, false),
                SpawnHaze(64, 4, 2, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(3);
            Assert.IsNull(newDisp);

            // OK
            disp = MakeDisposition(0,
                SpawnHaze(61, 21, 0, false),
                SpawnHaze(64, 4, 0, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(3);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(64, newDisp.hazes[0].pos);
        }

        [Test]
        public void TestSpareCarrotFail()
        {
            // not on carrot
            var disp = MakeDisposition(0,
                SpawnHaze(62, 33, 0, false),
                SpawnHaze(64, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(TurnChecker.CmdGiveCarrots);
            Assert.IsNull(newDisp);

            // to few carrots
            disp = MakeDisposition(0,
                SpawnHaze(59, 8, 0, false),
                SpawnHaze(64, 4, 2, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(TurnChecker.CmdGiveCarrots);
            Assert.IsNull(newDisp);
        }

        [Test]
        public void TestSpareCarrotOk()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(59, 33, 0, false),
                SpawnHaze(64, 4, 2, false));
            var checker = new TurnChecker(map, disp);
            var newDisp = checker.CheckTurnAndUpdate(TurnChecker.CmdGiveCarrots);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(23, newDisp.hazes[0].carrots);
            
            disp = MakeDisposition(0,
                SpawnHaze(59, 33, 0, false),
                SpawnHaze(64, 4, 2, false));
            checker = new TurnChecker(map, disp);
            newDisp = checker.CheckTurnAndUpdate(TurnChecker.CmdTakeCarrots);
            Assert.IsNotNull(newDisp);
            Assert.AreEqual(43, newDisp.hazes[0].carrots);
        }        
    }
}
