﻿using System;
using System.Text;
using HazeIgel.Model;
using HazeIgel.Model.AI;

namespace HazeIgelTest
{
    public class TurnMakerChainTest : TurnCheckerBaseTest
    {
        protected StepSeriesTestStatistics CheckStepSeries(Disposition disp, int turns,
            bool pruning, bool sorting, int depth)
        {
            var stepsBuilder = new StringBuilder();
            var stat = new StepSeriesTestStatistics
            {
                steps = turns
            };

            for (var i = 0; i < turns; i++)
            {
                var maker = new TurnMaker(disp, map, desc)
                {
                    aiSettings = new AiSettings
                    {
                        maxLevel = depth,
                        enableSorting = sorting,
                        enablePruning = pruning
                    }
                };
                var step = maker.CalculateTurn();
                stat.nodes += maker.statistics.nodes;
                stat.estimations += maker.statistics.estimations;
                stat.nodesMax = Math.Max(maker.statistics.nodes, stat.nodesMax);
                stat.estimationsMax = Math.Max(maker.statistics.estimations, stat.estimationsMax);
                stat.bestVector = maker.finalVector;

                stepsBuilder.Append($"hase[{disp.activeHaze}]: {step}; ");
                var checker = new TurnChecker(map, disp, desc);
                disp = checker.CheckTurnAndUpdate(step);
                disp.UpdatePositions(map);
                stat.finalPos = disp;
            }
            stat.StopTimer();
            stat.stepsRecord = stepsBuilder.ToString();
            return stat;
        }
    }

    public class StepSeriesTestStatistics
    {
        public int steps;

        public int estimations;

        public int nodes;

        public int estimationsMax;

        public int nodesMax;

        public int[] bestVector;

        public int milsSpent;

        public string stepsRecord;

        public Disposition finalPos;

        private DateTime timeStarted = DateTime.Now;

        public void StopTimer()
        {
            milsSpent = (int)(DateTime.Now - timeStarted).TotalMilliseconds;
        }

        public bool HaveSameFinalVectors(StepSeriesTestStatistics stat)
        {
            for (var i = 0; i < bestVector.Length; i++)
                if (bestVector[i] != stat.bestVector[i])
                    return false;
            return true;
        }

        public override string ToString()
        {
            return $"{steps} steps, took {milsSpent} ms. Average: {nodes / steps} nodes, max: {nodesMax} nodes\n" +
                   stepsRecord + "\nBest vector: [" + string.Join(", ", bestVector) + "]";
        }
    }
}
