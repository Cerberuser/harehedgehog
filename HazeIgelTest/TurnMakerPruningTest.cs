﻿using System;
using System.Linq;
using HazeIgel.Model;
using HazeIgel.Model.AI;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class TurnMakerPruningTest : TurnMakerChainTest
    {
        [Test]
        public void TestAlphaBetaWoSort()
        {
            var testDispositions = new[]
            {
                MakeDisposition(0, SpawnHaze(0, 68, 3, false), SpawnHaze(0, 68, 3, false)),
                MakeDisposition(0, SpawnHaze(45, 26, 2, false), SpawnHaze(29, 19, 0, false)),
                MakeDisposition(0, SpawnHaze(10, 100, 2, false), SpawnHaze(20, 10, 2, false))
            };

            for (var i = 0; i < testDispositions.Length; i++)
            {
                var statA = CheckStepSeries(testDispositions[i], 1, false, false, 6);
                var statB = CheckStepSeries(testDispositions[i], 1, true, false, 6);
                Assert.IsTrue(statB.HaveSameFinalVectors(statA));
                Assert.AreEqual(statA.stepsRecord, statB.stepsRecord);
                Assert.Less(statB.nodes, statA.nodes);
            }
        }

        [Test]
        public void TestAlphaBetaWithSort()
        {
            var testDispositions = new[]
            {
                MakeDisposition(0, SpawnHaze(0, 68, 3, false), SpawnHaze(0, 68, 3, false)),
                MakeDisposition(0, SpawnHaze(45, 26, 2, false), SpawnHaze(29, 19, 0, false)),
                MakeDisposition(0, SpawnHaze(10, 100, 2, false), SpawnHaze(20, 10, 2, false))
            };

            for (var i = 0; i < testDispositions.Length; i++)
            {
                var statA = CheckStepSeries(testDispositions[i], 1, true, false, 6);
                var statB = CheckStepSeries(testDispositions[i], 1, true, true, 6);
                Assert.IsTrue(statB.HaveSameFinalVectors(statA));
                Assert.Less(statB.nodes, statA.nodes);
            }
        }

        [Test]
        public void TestStepSeriesTheSameWoSorting()
        {
            var testDispositions = new[]
            {
                MakeDisposition(0, SpawnHaze(0, 68, 3, false), SpawnHaze(0, 68, 3, false)),
                MakeDisposition(0, SpawnHaze(45, 26, 2, false), SpawnHaze(29, 19, 0, false)),
                MakeDisposition(0, SpawnHaze(10, 100, 2, false), SpawnHaze(20, 10, 2, false))
            };

            for (var i = 0; i < testDispositions.Length; i++)
            {
                var statA = CheckStepSeries(testDispositions[i], 10, false, false, 6);
                var statB = CheckStepSeries(testDispositions[i], 10, true, false, 6);
                Assert.AreEqual(statA.stepsRecord, statB.stepsRecord);                
            }
        }

        [Test]
        public void CompareWithAndWoRestriction()
        {
            var disp = MakeDisposition(0, SpawnHaze(0, 68, 3, false), SpawnHaze(0, 68, 3, false));
            var makerBuilders = new Func<Disposition, TurnMaker>[]
            {
                p => new TurnMaker(p, map, desc)
                {
                    aiSettings = new AiSettings
                    {
                        enablePruning = true,
                        enableSorting = true,
                        maxLevel = 8
                    }
                },
                p => new TurnMaker(p, map, desc)
                {
                    aiSettings = new AiSettings
                    {
                        enablePruning = true,
                        enableSorting = true,
                        maxLevel = 9,
                        pruneRestrictionByLevel = new [] { 0, 0, 8, 8, 6, 6, 3, 3, 3, 3 }
                    }
                }
            };

            var hasWinner = false;
            var totalMilsSpent = new int[2];
            var turnsMade = 0;
            for (; turnsMade < 44; turnsMade++)
            {
                var makIndex = turnsMade & 1;
                var maker = makerBuilders[makIndex](disp);
                var start = DateTime.Now;
                var step = maker.CalculateTurn();
                totalMilsSpent[makIndex] += (int) (DateTime.Now - start).TotalMilliseconds;
                var checker = new TurnChecker(map, disp, desc);
                disp = checker.CheckTurnAndUpdate(step);
                if (disp.hazes.Any(h => h.Finished))
                {
                    hasWinner = true;
                    break;
                }
            }
            turnsMade++;
            var n2 = turnsMade / 2;
            var n1 = turnsMade - n2;

            var avgTime1 = totalMilsSpent[0] / n1;
            var avgTime2 = totalMilsSpent[1] / n2;
        }
    }    
}
