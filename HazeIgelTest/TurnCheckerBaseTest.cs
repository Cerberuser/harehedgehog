﻿using System.Collections.Generic;
using System.Linq;
using HazeIgel.Model;
using HazeIgel.Util;

namespace HazeIgelTest
{
    public class TurnCheckerBaseTest
    {
        protected FieldMap map;

        protected FieldMapDescriptor desc;

        public TurnCheckerBaseTest()
        {
            ExecutablePath.InitializeFake(string.Empty);
            map = FieldMap.Instance;
            desc = new FieldMapDescriptor(map);
        }

        protected Disposition MakeDisposition(int activeHaze, params Haze[] hazes)
        {
            var disp = new Disposition
            {
                hazes = hazes,
                activeHaze = activeHaze,
            };
            disp.UpdatePositions(map);
            return disp;
        }

        protected Haze SpawnHaze(int pos, int carrots, int salad, bool waits)
        {
            return new Haze(0)
            {
                pos = pos,
                carrots = carrots,
                salad = salad,
                waits = waits
            };
        }

        protected bool HasExactSameItems<T>(IEnumerable<T> colA, IEnumerable<T> colB)
        {
            if (colA.Count() != colB.Count())
                return false;
            var hashA = new HashSet<T>(colA);
            foreach (var b in colB)
                if (!hashA.Contains(b)) return false;
            return true;
        }
    }
}
