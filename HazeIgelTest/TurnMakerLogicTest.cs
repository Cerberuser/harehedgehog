﻿using HazeIgel.Model.AI;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class TurnMakerLogicTest : TurnMakerChainTest
    {
        [Test]
        public void TestSimpleStep()
        {
            // simply compare position
            var disp = MakeDisposition(0,
                SpawnHaze(45, 10, 2, false),
                SpawnHaze(44, 10, 2, false));

            var maker = new TurnMaker(disp, map, desc);
            var step = maker.CalculateTurn();
            Assert.AreNotEqual(0, step);
        }

        [Test]
        public void TestFinish()
        {
            var disp = MakeDisposition(1,
                SpawnHaze(13, 357, 1, false),
                SpawnHaze(59, 20, 0, false));

            var maker = new TurnMaker(disp, map, desc);
            var step = maker.CalculateTurn();
            Assert.AreEqual(5, step);
        }

        [Test]
        public void TestFinishAgain()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(59, 17, 0, false),
                SpawnHaze(51, 43, 0, false));
            var maker = new TurnMaker(disp, map, desc);
            var step = maker.CalculateTurn();
            Assert.AreEqual(5, step);
        }

        [Test]
        public void TestSkipHase()
        {
            var disp = MakeDisposition(0, 
                SpawnHaze(49, 48, 0, false), 
                SpawnHaze(54, 16, 0, false));
            var stat = CheckStepSeries(disp, 5, true, true, 7);
            Assert.IsTrue(stat.finalPos.hazes[0].Finished);
        }

        [Test]
        public void TestMittelspielTurn()
        {
            var disp = MakeDisposition(1,
                SpawnHaze(59, 23, 0, false),
                SpawnHaze(47, 66, 1, false),
                SpawnHaze(50, 78, 1, false));
            var maker = new TurnMaker(disp, map,
                desc)
            {
                aiSettings = new AiSettings
                {
                    pruneRestrictionByLevel = new[] { 0, 0, 8, 8, 6, 6, 3, 3, 3, 3 },
                    maxLevel = 9
                }
            };
            var step = maker.CalculateTurn();
        }
    }
}
