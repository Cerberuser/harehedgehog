﻿function Game() {
    var that = this;
    this.map = new Map();
    this.map.onCellHover = function(cell) { that.onCellHover(cell); };
    this.map.onCellClick = function (cell) { that.onCellClick(cell); };
    this.apiUri = 'https://localhost:44365/api/game/turn';
    this.gameState = null;
    this.gameStat = new GameStat();
    this.setupCssSelectors();
    this.computerPlayers = [];
    this.estimation = new Estimation();
    this.lastSample = -1;

    this.showEstimationPane = getParameterByName('estimation') != null;
    if (!this.showEstimationPane)
        $(this.paneEstimation).hide();
}

Game.prototype.setupCssSelectors = function() {
    this.playerTurnDivSelector = 'div#divPlayerTurn';
    this.counterSaladSelector = '#counterSalad';
    this.counterCarrotSelector = '#counterCarrot';

    this.btnGiveCarrotsSelector = 'a#actionGiveCarrots';
    this.btnTakeCarrotsSelector = 'a#actionTakeCarrots';
    this.btnWaitSelector = 'a#actionWait';

    this.modalHaseEffectsSelector = '#dialogHaseEffect';
    this.modalHaseEffectsTextSelector = 'div#haseEffectsTextHolder';

    this.modalNewGameSelector = '#dialogNewGame';

    this.btnEstimWin = '#btnEstimWin';
    this.btnEstimLos = '#btnEstimLos';
    this.turnNumberSpan = '#turnNumberSpan';
    this.paneEstimation = '#paneEstimation';

    this.paneHint = '#haseStateHint';
}

Game.prototype.init = function () {
    var state = this.loadSavedState();
    if (state != null) {
        this.ensureComputerPlayers();
        this.renderGameState(state);
        return;
    }
    this.openNewGameDialog();
}

Game.prototype.ensureComputerPlayers = function (humans, computers) {
    if (typeof humans !== "undefined") {
        this.computerPlayers = [];
        var total = humans + computers;
        for (var i = 0; i < total; i++) {
            if ((i & 1) === 0) {
                this.computerPlayers.push(humans === 0);
                humans--;
            } else {
                this.computerPlayers.push(computers > 0);
                computers--;
            }
        }
    } else {
        var json = window.localStorage['computerPlayers'];
        try {
            this.computerPlayers = JSON.parse(json);
        } catch (e) {}
        if (!this.computerPlayers || !this.computerPlayers.length)
            this.computerPlayers = [false, false, false, false, false, false];
    }
    // save
    window.localStorage['computerPlayers'] = JSON.stringify(this.computerPlayers);
    this.map.computerPlayers = this.computerPlayers;
}

Game.prototype.startNewGame = function(humanPlayers, aiPlayers) {
    var that = this;
    this.performCmd(
        { humanPlayers: humanPlayers, aiPlayers: aiPlayers, ClassName: 'GameCommandNewGame' },
        function (data) {
            if (!data.success) {
                console.log('Error processing turn: ' + data.error);
                return;
            }
            that.ensureComputerPlayers(humanPlayers, aiPlayers);
            that.gameStat.clear();
            that.renderGameState({
                disposition: data.newDisposition,
                possibleSteps: data.possibleSteps
            });
        });
}

Game.prototype.action = function(act) {
    if (act === 'give') this.makeTurn(1002);
    if (act === 'take') this.makeTurn(1001);
    if (act === 'wait') this.makeTurn(0);
}

Game.prototype.makeTurn = function(command) {
    var that = this;
    this.performCmd({
            disp: this.gameState.disposition,
            command: command,
            ClassName: 'GameCommandPlayerTurn'
        },
        function (data) {
            if (!data.success) {
                alert(data.error);
                return;
            }
            that.renderGameState({
                disposition: data.newDisposition,
                possibleSteps: data.possibleSteps
            });
        });
}

Game.prototype.makeAiTurn = function () {
    var that = this;
    this.performCmd({
            disp: this.gameState.disposition,
            ClassName: 'GameCommandAiTurn'
        },
        function (data) {
            if (!data.success) {
                alert(data.error);
                return;
            }
            that.gameStat.parseAndAddRecord(data.extra);
            that.renderGameState({
                disposition: data.newDisposition,
                possibleSteps: data.possibleSteps
            });
        });
}

Game.prototype.onCellHover = function(cell) {
    var step = this.findPossibleStep(cell);
    if (!step) {
        this.renderSaladCarrots(0);
        return;
    }    
    var extraCarrots = step.newState.carrots - this.gameState.disposition.ActiveHaze.carrots;
    this.renderSaladCarrots(extraCarrots);
}

Game.prototype.openNewGameDialog = function() {
    var that = this;

    $(this.modalNewGameSelector + ' span.modal-close, ' +
        this.modalNewGameSelector + ' a.modal-cancel').on('click', () => {
            $(that.modalNewGameSelector).css('display', 'none');
        });
    $(this.modalNewGameSelector + ' a.modal-ok').on('click', () => {
        $(that.modalNewGameSelector).css('display', 'none');
        that.onPlayerChoseNewGame();
    });

    $(this.modalNewGameSelector).css('display', 'block');    
}

Game.prototype.onPlayerChoseNewGame = function() {
    var humans = parseInt($('#newGameInputHuman').val());
    var computers = parseInt($('#newGameInputComputer').val());
    if (humans < 0 ||
        computers < 0 ||
        (humans + computers) < 2 ||
        (humans + computers) > 6) {
        alert('Please, select humans / computer players to be 2 .. 6 people totally');
        return;
    }
    //console.log(humans + ', ' + computers);
    this.startNewGame(humans, computers);
}

Game.prototype.showHaseModal = function() {
    var text = this.gameState.disposition.haseEffects;
    $(this.modalHaseEffectsTextSelector).text(text);
    var that = this;
    
    $(this.modalHaseEffectsSelector + ' span.modal-close, ' +
        this.modalHaseEffectsSelector + ' a.modal-cancel').on('click', () => {
        $(that.modalHaseEffectsSelector).css('display', 'none');
    });

    $(this.modalHaseEffectsSelector).css('display', 'block');
}

Game.prototype.onCellClick = function (cell) {
    var step = this.findPossibleStep(cell);
    if (!step) return;
    this.makeTurn(step.command);
}

Game.prototype.findPossibleStep = function(cell) {
    if (!this.gameState || !this.gameState.possibleSteps) return null;
    var stepCmd = cell - this.gameState.disposition.ActiveHaze.pos;
    return findByPropVal(this.gameState.possibleSteps, 'command', stepCmd);
}

Game.prototype.renderGameState = function (state) {
    this.gameState = state;
    // save
    window.localStorage['gameState'] = JSON.stringify(state);
    this.map.placePlayers(this.gameState.disposition.hazes);
    this.highlightCells();
    this.showPlayerTurn();
    this.renderSaladCarrots(0);
    this.enableActionButtons();
    var turn = this.gameState.disposition.turn;
    $(this.turnNumberSpan).text(turn);

    var eftcs = this.gameState.disposition.haseEffects;
    if (eftcs && eftcs.length)
        this.showHaseModal();
    this.renderHint();
}

Game.prototype.renderHint = function() {
    var text = '';
    var hazes = this.gameState.disposition.hazes;
    for (var i = 0; i < hazes.length; i++) {
        var isActive = i === this.gameState.disposition.activeHaze;
        if (isActive)
            text += '<b>';
        text += '@' + hazes[i].pos + ', ' + hazes[i].carrots + ' carrots, ' +
            hazes[i].salad + ' salad';
        if (hazes[i].waits)
            text += ', waits';
        if (isActive)
            text += '</b>';
        text += '<br/>';
    }
    $(this.paneHint).html(text);
}

Game.prototype.enableActionButtons = function() {
    var enabledClass = 'ref-btn';
    var disabledClass = 'ref-btn-pale';
    var actions = this.gameState.possibleSteps;
    var states = [checkByPropVal(actions, 'command', 1002),
        checkByPropVal(actions, 'command', 0), checkByPropVal(actions, 'command', 1001)];
    var btns = [this.btnGiveCarrotsSelector, this.btnWaitSelector, this.btnTakeCarrotsSelector];
    for (var i = 0; i < states.length; i++) {
        var cls = states[i] ? enabledClass : disabledClass;
        var old = !states[i] ? enabledClass : disabledClass;
        $(btns[i]).removeClass(old);
        $(btns[i]).addClass(cls);
    }
}

Game.prototype.renderSaladCarrots = function (extraCarrot) {
    if (!this.gameState) return;
    var carrots = this.gameState.disposition.ActiveHaze.carrots + '';
    var salad = this.gameState.disposition.ActiveHaze.salad + '';
    if (extraCarrot)
        carrots += ' (' + extraCarrot + ')';
    $(this.counterCarrotSelector).text(carrots);
    $(this.counterSaladSelector).text(salad);
}

Game.prototype.highlightCells = function() {
    var highIndices = {};
    for (var i = 0; i < this.gameState.possibleSteps.length; i++) {
        var step = this.gameState.possibleSteps[i].command;
        if (step > 100) continue;
        highIndices[this.gameState.disposition.ActiveHaze.pos + step] = true;
    }
    this.map.highlightCells(highIndices, this.gameState.disposition.activeHaze);
}

Game.prototype.loadSavedState = function() {
    var state = window.localStorage['gameState'];
    if (!state || !state.length) return null;
    try {
        return JSON.parse(state);
    } catch (ex) {
        return null;
    }
}

Game.prototype.performCmd = function (cmd, handler) {
    $.ajax({
        type: 'POST',
        url: this.apiUri,
        data: "'" + JSON.stringify(cmd) + "'",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        error(er) {
            console.log(er);
        }
    }).done(function(data) {
        handler(data);
    });
}

Game.prototype.showPlayerTurn = function() {
    var parDiv = $(this.playerTurnDivSelector);
    var markup = '';
    for (var i = 0; i < this.gameState.disposition.hazes.length; i++) {
        var selected = i === this.gameState.disposition.activeHaze;
        var haze = this.gameState.disposition.hazes[i];
        var preffix = this.isComputerPlayer(i) ? 'computer' : 'player';
        var image = preffix + '_' + (i + 1) + '_' + (haze.waits ? 'w' : 'a');
        var width = selected ? 40 : 20;
        var height = width * 3 / 2;
        image = '<img src="img/' + image + '.png" style="width:' + width + 
            'px; height:' + height + 'px"/> ';
        markup += image;
    }
    parDiv.html(markup);
}

Game.prototype.isComputerPlayer = function(index) {
    return (this.computerPlayers &&
        this.computerPlayers.length > index &&
        this.computerPlayers[index]);
}

Game.prototype.loadSample = function() {
    var index = parseInt($('#inputSampleIndex').val());
    this.loadSampleByIndex(index);
    $('#inputSampleIndex').val((index + 1) + '');
}

Game.prototype.loadSampleByIndex = function (index) {
    var that = this;
    this.performCmd({
            sample: index,
            ClassName: 'GameCommandLoadSample'
        },
        function (data) {
            if (!data.success) {
                alert(data.error);
                return;
            }
            that.lastSample = index;
            var h = data.newDisposition.hazes[1];
            console.log('Rival: @' + h.pos + ', ' + h.carrots + ' carts, ' + h.salad + ' salad');
            that.stressWinLoseBtn(data.extra);
            that.renderGameState({
                disposition: data.newDisposition,
                possibleSteps: data.possibleSteps
            });
        });
}

Game.prototype.estimate = function(wl) {
    if (this.lastSample < 0) return;
    this.performCmd({
            sample: this.lastSample,
            winner: wl,
            ClassName: 'GameCommandUpdateSample'
        },
        function (data) {
            if (!data.success) {
                alert(data.error);
                return;
            }
            console.log('estimated');
        });
}

Game.prototype.stressWinLoseBtn = function (extra) {
    var selBtnClass = 'btn-selected';
    if (extra == '1')
        $(this.btnEstimWin).addClass(selBtnClass);
    else
        $(this.btnEstimWin).removeClass(selBtnClass);
    if (extra == '2')
        $(this.btnEstimLos).addClass(selBtnClass);
    else
        $(this.btnEstimLos).removeClass(selBtnClass);
}

function findByPredicate(ar, predicat) {
    for (var i = 0; i < ar.length; i++)
        if (predicat(ar[i])) return ar[i];
    return null;
}

function findByPropVal(ar, propName, val) {
    for (var i = 0; i < ar.length; i++)
        if (ar[i][propName] === val) return ar[i];
    return null;
}

function checkByPropVal(ar, propName, val) {
    for (var i = 0; i < ar.length; i++)
        if (ar[i][propName] === val) return true;
    return false;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}