﻿function GameStat() {
    this.records = [];
}

GameStat.prototype.clear = function() {
    this.records = [];
}

GameStat.prototype.parseAndAddRecord = function(json) {
    if (!json || !json.length) return;
    var ar = JSON.parse(json);
    this.records.push(ar);
}

GameStat.prototype.printStatInConsole = function() {
    console.log('turn;total;per node;estimates');
    for (var i = 0; i < this.records.length; i++)
        console.log(i + ';' + this.records[i].join(';'));
}