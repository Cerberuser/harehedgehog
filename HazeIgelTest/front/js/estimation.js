﻿function Estimation() {
    this.samples = [];
    this.setupSamples();
}

Estimation.prototype.loadSample = function(index) {
    if (index < 0)        
        return { success: false, error: 'negative index' };
    if (index >= this.samples.length)
        return { success: false, error: 'index is greater than max' };
    
    return {
        disp: this.makeSampleTurnObj(index),
        success: true
    };
}

Estimation.prototype.makeSampleTurnObj = function(i) {
    var sample = this.samples[i];
    var disp = {
        activeHaze: 0,
        hazes: sample
    };
    return disp;
}

Estimation.prototype.setupSamples = function() {
    var data =
        '45;26;2;f;29;19;0;f\n' +
        '10;100;2;f;20;10;2;f\n' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '';
}

Estimation.prototype.parseSamples = function(str) {
    var lines = str.split('\n');
    for (var i = 0; i < lines.length; i++) {
        if (!lines[i] || !lines[i].length) continue;
        var parts = line.split(';');
        if (parts.length !== 8) continue;
        this.samples.push([
            {
                pos: parseInt(parts[0]),
                carrots: parseInt(parts[1]),
                salad: parseInt(parts[2]),
                waits: parts[3] === 't'
            },
            {
                pos: parseInt(parts[4]),
                carrots: parseInt(parts[5]),
                salad: parseInt(parts[6]),
                waits: parts[7] === 't'
            }
        ]);
    }
}