﻿namespace HazeIgel.Model.Game
{
    /// <summary>
    /// request for making a move by AI
    /// AI choses the best move for disp.ActiveHaze and makes it
    /// </summary>
    public class GameCommandAiTurn : GameCommand
    {
        /// <summary>
        /// current disposition
        /// </summary>
        public Disposition disp;

        public GameCommandAiTurn()
        {
            ClassName = "GameCommandAiTurn";
        }
    }
}
