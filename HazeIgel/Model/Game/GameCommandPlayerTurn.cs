﻿namespace HazeIgel.Model.Game
{
    /// <summary>
    /// make a move by the command for the disp.ActiveHaze
    /// the command is provided by a human player
    /// </summary>
    public class GameCommandPlayerTurn : GameCommand
    {
        /// <summary>
        /// current disposition
        /// </summary>
        public Disposition disp;

        /// <summary>
        /// command: 0, 1, 2 ..., -1, -2, ... or [1001, 1002]
        /// </summary>
        public int command;

        public GameCommandPlayerTurn()
        {
            ClassName = "GameCommandPlayerTurn";
        }        
    }
}
