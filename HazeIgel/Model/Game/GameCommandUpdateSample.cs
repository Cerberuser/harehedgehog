﻿namespace HazeIgel.Model.Game
{
    /// <summary>
    /// update "etude", set and store the estimate made by human
    /// used when solving "etudes"
    /// </summary>
    public class GameCommandUpdateSample : GameCommand
    {
        /// <summary>
        /// sample disposition index
        /// </summary>
        public int sample;

        /// <summary>
        /// human's estimate:
        ///  1 - player[0] wins
        ///  2 - player[1] wins
        /// </summary>
        public int winner;

        public GameCommandUpdateSample()
        {
            ClassName = "GameCommandUpdateSample";
        }
    }
}
