﻿using System;
using System.Globalization;
using System.Linq;
using HazeIgel.Model.AI;
using HazeIgel.Model.Estimation;

namespace HazeIgel.Model.Game
{
    /// <summary>
    /// used in REST API
    ///  parses a command from JSON
    ///  executes the command
    ///  returns universal response
    /// </summary>
    public class GameController
    {
        public static CommandResponse ProcessCommand(string commandJson)
        {
            GameCommand cmd;
            try
            {
                cmd = GameCommand.ParseCommand(commandJson);
            }
            catch (Exception e)
            {
                return new CommandResponse
                {
                    error = $"{e.GetType().Name}: {e.Message}"
                };
            }
            if (cmd is GameCommandNewGame)
                return ProcessNewGameCmd((GameCommandNewGame) cmd);
            if (cmd is GameCommandPlayerTurn)
                return ProcessPlayerTurnCmd((GameCommandPlayerTurn)cmd);
            if (cmd is GameCommandAiTurn)
                return ProcessAITurnCommand((GameCommandAiTurn)cmd);
            if (cmd is GameCommandLoadSample)
                return ProcessGameCommandLoadSample((GameCommandLoadSample)cmd);
            if (cmd is GameCommandUpdateSample)
                return ProcessGameCommandUpdateSample((GameCommandUpdateSample)cmd);

            return new CommandResponse
            {
                error = $"not implemented"
            };
        }

        public static CommandResponse ProcessNewGameCmd(GameCommandNewGame cmd)
        {
            var players = cmd.aiPlayers + cmd.humanPlayers;
            if (players < 2)
                return new CommandResponse
                {
                    error = "to few players"
                };
            if (players > 6)
                return new CommandResponse
                {
                    error = "to many players"
                };
            var disp = new Disposition
            {
                hazes = Enumerable.Range(0, players).Select(i => new Haze(i)
                {
                    carrots = TurnChecker.DefaultCarrots,
                    salad = TurnChecker.DefaultSalad,
                    pos = 0,
                    waits = false
                }).ToArray()
            };
            disp.UpdatePositions(FieldMap.Instance);
            var checker = new TurnChecker(FieldMap.Instance, disp);
            var turns = checker.GetExplainedSteps();
            return new CommandResponse
            {
                success = true,
                newDisposition = disp,
                possibleSteps = turns
            };
        }

        public static CommandResponse ProcessPlayerTurnCmd(GameCommandPlayerTurn cmd)
        {
            if (cmd.disp == null)
                return new CommandResponse
                {
                    error = "disposition was not provided"
                };

            cmd.disp.UpdatePositions(FieldMap.Instance);
            var checker = new TurnChecker(FieldMap.Instance, cmd.disp);
            var newDisp = checker.CheckTurnAndUpdate(cmd.command);
            if (newDisp == null)
                return new CommandResponse
                {
                    error = "can't make this turn"
                };

            checker = new TurnChecker(FieldMap.Instance, newDisp);
            var posSteps = checker.GetExplainedSteps();
            return new CommandResponse
            {
                success = true,
                newDisposition = newDisp,
                possibleSteps = posSteps
            };
        }

        public static CommandResponse ProcessAITurnCommand(GameCommandAiTurn cmd)
        {
            if (cmd.disp == null)
                return new CommandResponse
                {
                    error = "disposition was not provided"
                };

            cmd.disp.UpdatePositions(FieldMap.Instance);
            var desc = new FieldMapDescriptor(FieldMap.Instance);
            var maker = new TurnMaker(cmd.disp, FieldMap.Instance,
                desc)
            {
                aiSettings = new AiSettings
                {
                    maxLevel = 9,
                    pruneRestrictionByLevel = new[] { 0, 0, 8, 8, 6, 6, 3, 3, 3, 3 },
                    // Great Leslie or Professor Fate?
                    contractFunc = EstimateContractingFun.AbsoluteMaximum
                }
            };
            var step = maker.CalculateTurn();

            var checker = new TurnChecker(FieldMap.Instance, cmd.disp, desc);
            var newDisp = checker.CheckTurnAndUpdate(step);
            if (newDisp == null)
                return new CommandResponse
                {
                    error = $"can't make this turn ({step})"
                };

            checker = new TurnChecker(FieldMap.Instance, newDisp);
            var posSteps = checker.GetExplainedSteps();
            return new CommandResponse
            {
                success = true,
                newDisposition = newDisp,
                possibleSteps = posSteps,
                extra = "[" + string.Join(", ", new []
                {
                    maker.statistics.nodes,
                    maker.statistics.estimations,
                    0
                }.Select(n => n.ToString(CultureInfo.InvariantCulture))) + "]"
            };
        }

        public static CommandResponse ProcessGameCommandLoadSample(GameCommandLoadSample cmd)
        {
            var sample = new EstimationStore().GetSample(cmd.sample);
            if (sample == null)
                return new CommandResponse
                {
                    error = "incorrect sample index"
                };
            var disp = new Disposition
            {
                hazes = sample.hazes,
                activeHaze = 0
            };
            disp.UpdatePositions(FieldMap.Instance);
            var checker = new TurnChecker(FieldMap.Instance, disp);
            var steps = checker.GetExplainedSteps();

            return new CommandResponse
            {
                success = true,
                newDisposition = disp,
                possibleSteps = steps,
                extra = sample.winner.ToString()
            };
        }

        public static CommandResponse ProcessGameCommandUpdateSample(GameCommandUpdateSample cmd)
        {
            var stor = new EstimationStore();
            var sample = stor.GetSample(cmd.sample);
            if (sample == null)
                return new CommandResponse
                {
                    error = "incorrect sample index"
                };
            stor.UpdateSample(cmd.sample, cmd.winner);
            return new CommandResponse
            {
                success = true                
            };
        }
    }
}
