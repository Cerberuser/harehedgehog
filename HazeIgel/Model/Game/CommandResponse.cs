﻿using System.Collections.Generic;

namespace HazeIgel.Model.Game
{
    /// <summary>
    /// a universal response for some GameCommand
    /// used in REST API
    /// </summary>
    public class CommandResponse
    {
        /// <summary>
        /// was the command successfuly processed?
        /// </summary>
        public bool success;

        /// <summary>
        /// optional error string
        /// </summary>
        public string error;

        /// <summary>
        /// some extra information, for debugging etc
        /// </summary>
        public string extra;

        /// <summary>
        /// new (resulted) game position
        /// </summary>
        public Disposition newDisposition;

        /// <summary>
        /// possible moves for the current (players[newDisp.activeHaze]) player,
        /// with detail on each move
        /// </summary>
        public List<ExplainedStep> possibleSteps;
    }
}
