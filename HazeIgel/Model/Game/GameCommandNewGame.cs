﻿namespace HazeIgel.Model.Game
{
    /// <summary>
    /// make initial disposition for the provided numbers
    /// of human and computer players, give all the possible
    /// steps to the first player
    /// </summary>
    public class GameCommandNewGame : GameCommand
    {
        public GameCommandNewGame()
        {
            ClassName = "GameCommandNewGame";
        }

        public int humanPlayers;

        public int aiPlayers;
    }
}
