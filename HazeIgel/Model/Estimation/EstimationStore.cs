﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HazeIgel.Util;

namespace HazeIgel.Model.Estimation
{
    /// <summary>
    /// used to read "etudes" (dispositions) from a file,
    /// to update human's estimation for an etude
    /// </summary>
    public class EstimationStore
    {
        private readonly List<EstimationSample> samples = new List<EstimationSample>();

        public EstimationStore()
        {
            LoadSamples();
        }

        public EstimationSample GetSample(int index)
        {
            return index < 0 || index >= samples.Count ? null : samples[index];
        }

        public List<EstimationSample> GetSamples()
        {
            return samples;
        }

        /// <summary>
        /// set the "winner" variable and save "etudes" back to the file
        /// </summary>
        public void UpdateSample(int index, int winner)
        {
            samples[index].winner = winner;
            var path = ExecutablePath.Combine("functions", "estimation.csv");
            using (var sw = new StreamWriter(path, false, Encoding.ASCII))
            {
                foreach (var sm in samples)
                    sw.WriteLine(sm);
            }
        }

        /// <summary>
        /// read "etudes" from a CSV file
        /// </summary>
        private void LoadSamples()
        {
            var path = ExecutablePath.Combine("functions", "estimation.csv");
            using (var sr = new StreamReader(path, Encoding.ASCII))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line)) continue;
                    var sm = EstimationSample.ParseLine(line);
                    if (sm != null)
                        samples.Add(sm);
                }
            }
        }
    }

    /// <summary>
    /// a disposition for 2 player, the first player is to make a move
    /// 
    /// - winner is 0 if the disposition was not yet estimated by a human
    /// - winner is 1 when the human believes the first (0) player will win
    /// - winner is 2 when the human believes the second (1) player will win
    /// </summary>
    public class EstimationSample
    {
        /// <summary>
        /// hase's position, the first is one who's making a move
        /// </summary>
        public Haze[] hazes;

        public int winner;

        public static EstimationSample ParseLine(string line)
        {
            var parts = line.Split(';', StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 9)
                return null;
            return new EstimationSample
            {
                hazes = new[]
                {
                    new Haze
                    {
                        pos = int.Parse(parts[0]),
                        carrots = int.Parse(parts[1]),
                        salad = int.Parse(parts[2]),
                        waits = parts[3] == "t"
                    },
                    new Haze
                    {
                        pos = int.Parse(parts[4]),
                        carrots = int.Parse(parts[5]),
                        salad = int.Parse(parts[6]),
                        waits = parts[7] == "t"
                    }
                },
                winner = int.Parse(parts[8])
            };
        }

        public override string ToString()
        {
            var w0 = hazes[0].waits ? "t" : "f";
            var w1 = hazes[1].waits ? "t" : "f";
            return
                $"{hazes[0].pos};{hazes[0].carrots};{hazes[0].salad};{w0};{hazes[1].pos};{hazes[1].carrots};{hazes[1].salad};{w1};{winner}";
        }
    }
}
