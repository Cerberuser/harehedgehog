﻿namespace HazeIgel.Model
{
    /// <summary>
    /// base cell class, can be "carrot", "salad", "position",
    /// "hase", "igel", "start" or "finish" cell
    /// 
    /// most of the cell classes just "do" nothing
    /// 
    /// cells are stored in FieldMap.cells array
    /// </summary>
    public class BaseCell
    {
    }
}
