﻿namespace HazeIgel.Model
{
    /// <summary>
    /// constants for some game actions, calculations and commands
    /// </summary>
    public partial class TurnChecker
    {
        /// <summary>
        /// carrots per each step backward when leaping back on Igel cell
        /// </summary>
        public const int CarrotsPerLeapBack = 10;

        /// <summary>
        /// carrots per 1 salad cell, being multiplied on 
        /// current relative player's position
        /// </summary>
        public const int CarrotsPerSalad = 10;

        /// <summary>
        /// carrots given to the player when making move
        /// from a position cell, being multiplied on 
        /// current relative player's position
        /// </summary>
        public const int CarrotsPerPosition = 10;

        /// <summary>
        /// max carrots allowed when finishing, this value is
        /// also multiplied on current relative player's position
        /// (on finish)
        /// </summary>
        public const int CarrotsMaxOnFinish = 10;

        /// <summary>
        /// carrots given or taking when player stays on carrot cell
        /// </summary>
        public const int CarrotsPerWaiting = 10;

        /// <summary>
        /// special command code: stay on carrot cell, take 10 carrots
        /// </summary>
        public const int CmdTakeCarrots = 1001;

        /// <summary>
        /// special command code: stay on carrot cell, give 10 carrots
        /// </summary>
        public const int CmdGiveCarrots = 1002;

        /// <summary>
        /// default carrots count on start position
        /// </summary>
        public const int DefaultCarrots = 68;

        /// <summary>
        /// default salad cards count on start position
        /// </summary>
        public const int DefaultSalad = 3;
    }
}
