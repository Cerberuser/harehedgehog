﻿using HazeIgel.Model.AI; // used in cref

namespace HazeIgel.Model
{
    /// <summary>
    /// the attributes that are describing a player (hase)
    /// during the game
    /// </summary>
    public class Haze
    {
        /// <summary>
        /// special attribute being used in
        /// <see cref="TurnMaker"/> - at which position the player has finished
        /// (1st, 2nd, 3d, 4th, 5th or 6th) - if finished
        /// </summary>
        public int index;

        /// <summary>
        /// cell index on which the player resides
        /// </summary>
        public int pos;

        /// <summary>
        /// how much carrots player has
        /// </summary>
        public int carrots;

        /// <summary>
        /// how much salad cards player has
        /// </summary>
        public int salad;

        /// <summary>
        /// is player waiting because of salad or hase card effects
        /// or he just has nowhere to move
        /// </summary>
        public bool waits;

        /// <summary>
        /// is player on the finish cell
        /// </summary>
        public bool Finished => pos == FieldMap.Instance.fieldsCount - 1;

        public Haze()
        {
        }

        public Haze(int index)
        {
            this.index = index;
        }

        /// <summary>
        /// copy ctor
        /// </summary>
        public Haze(Haze h)
        {
            index = h.index;
            pos = h.pos;
            carrots = h.carrots;
            salad = h.salad;
            waits = h.waits;
        }

        public override string ToString()
        {
            var posStr = pos == 0 ? "start" : Finished ? "finish" : pos.ToString();
            var waitsStr = waits ? ", waits" : "";
            return $"Haze#{index} at {posStr}, {carrots} carrots, {salad} salad{waitsStr}";
        }
    }
}
