﻿using System.Collections.Generic;
using System.Linq;

namespace HazeIgel.Model
{
    /// <summary>
    /// the full state of a game
    /// specifically: the hazes' info (hazes) + 
    ///   the number of a hase to make a turn (activeHaze)
    /// 
    /// the other information (ActiveHaze, hazePos, occupiedPos, hazeEffects)
    /// is just for calculations / players' reference (haseEffects)
    /// </summary>
    public class Disposition
    {
        /// <summary>
        /// each hase's state
        /// </summary>
        public Haze[] hazes;

        /// <summary>
        /// the hase who's going to make its turn
        /// </summary>
        public int activeHaze;

        public Haze ActiveHaze => hazes[activeHaze];

        /// <summary>
        /// for calculations:
        /// [3, 1, 2]: hazes[0] is the last, hazes[1] is the first on the map
        /// </summary>
        public int hazePos;

        /// <summary>
        /// for calculations:
        /// map fields occupied (true) by a haze or free (false)
        /// </summary>
        public Dictionary<int, bool> occupiedPos;

        /// <summary>
        /// for player's reference:
        /// the effects implied by the haze's card on the previos turn
        /// </summary>
        public string haseEffects;

        /// <summary>
        /// turn's number, starting from 0
        /// </summary>
        public int turn;

        /// <summary>
        /// make a deep copy of Disposition
        /// </summary>
        public Disposition Clone()
        {
            var act = activeHaze + 1;
            if (act == hazes.Length) act = 0;
            return new Disposition
            {
                hazes = hazes.Select(h => new Haze(h)).ToArray(),
                activeHaze = act,
                turn = turn + 1
            };
        }

        /// <summary>
        /// calculate fields, that are being used for other calculations
        /// </summary>
        public void UpdatePositions(FieldMap map)
        {
            // occupied cells
            occupiedPos = Enumerable.Range(0, map.fieldsCount).ToDictionary(i => i, i => false);
            foreach (var haze in hazes)
                occupiedPos[haze.pos] = true;
            // haze's position
            var curPos = ActiveHaze.pos;
            hazePos = hazes.Count(h => h.pos >= curPos);
        }

        /// <summary>
        /// index of the player who made his turn before the current one
        /// </summary>
        /// <returns></returns>
        public int GetPreviousPlayerIndex()
        {
            return activeHaze == 0 ? hazes.Length - 1 : activeHaze - 1;
        }
    }
}
