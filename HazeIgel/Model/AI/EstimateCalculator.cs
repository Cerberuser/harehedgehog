﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using HazeIgel.Util;

namespace HazeIgel.Model.AI
{
    /// <summary>
    /// here we made an estimation for the disposition
    /// by heuristic only
    /// 
    /// this estimation is for future use while solving a Minimax tree
    /// the "function" EstimatePosition considers the following:
    /// - how far the player is from the start position
    /// - how many carrots and salad the player has
    /// - aren't carrots too much to finish?
    /// ...
    /// </summary>
    public class EstimateCalculator
    {
        /// <summary>
        /// map object, remains unchanged
        /// </summary>
        private readonly FieldMap map;

        /// <summary>
        /// disposition being estimated
        /// </summary>
        private readonly Disposition disp;

        /// <summary>
        /// player index, whose "chances" is estimated
        /// </summary>
        private int playerIndex;

        /// <summary>
        /// player by playerIndex
        /// </summary>
        private Haze haze;

        /// <summary>
        /// [player_salad][salad_cells_left_to_go]
        /// [0..3][0..5]
        /// stored in functions/func_salad.csv
        /// "salad_cells_left_to_go" stored in functions/salad_cells_left.csv
        /// </summary>
        private static int[][] saladFunction;

        /// <summary>
        /// used in saladFunction to get salad_cells_left from
        /// current player cell index
        /// </summary>
        private static int[] saladCellsLeftFunction;

        /// <summary>
        /// [player_rel_distance][player_carrots]
        /// player_rel_distance - 0 if less than 1/3 of map is passed,
        ///   1 if more than 1/3 passed, 2 if player has enough (or even to much) carrots to
        ///   go the finish cell
        /// </summary>
        private static int[][] carrotsFunction;

        /// <summary>
        /// correction factors for Salad, Carrot and Path components:
        /// F = coeffs[0] * S + coeffs[1] * C + coeffs[2] * P
        /// </summary>
        public float[] estimationCoeffs = {0.9f, 0.6f, 1.4f};

        /// <summary>
        /// does the player have eough carrots to reach the finish cell?
        /// </summary>
        private bool canReachFinish;

        static EstimateCalculator()
        {
            LoadFunctions();
        }

        public EstimateCalculator(FieldMap map, Disposition disp)
        {
            this.map = map;
            this.disp = disp;
        }

        /// <summary>
        /// estimate player[playerIndex] "chances", or how
        /// good or bad his position is
        /// </summary>
        public int EstimatePosition(int playerIndex)
        {
            this.playerIndex = playerIndex;
            haze = disp.hazes[playerIndex];
            var fSalad = CalculateSaladScore();
            var fCarrots = CalculateCarrotsScore();
            var fPath = CalculatePathScore();
            var estimate = (int)
                (estimationCoeffs[0] * fSalad + estimationCoeffs[1] * fCarrots + estimationCoeffs[2] * fPath);
            return estimate;
        }

        private int CalculatePathScore()
        {
            return haze.pos * 3;
        }

        private int CalculateCarrotsScore()
        {
            // if haze has finished
            if (haze.pos == map.fieldsCount - 1)
                return 800000 - 100000 * haze.index;
            
            var actualCarrots = CalculateActualCarrots();
            // calculate distance of one leap
            var leapDistance = FieldMap.GetMovesForCarrots(actualCarrots);
            canReachFinish = haze.pos + leapDistance >= map.fieldsCount - 1;

            var distanceIndex = canReachFinish ? 2 : haze.pos > map.fieldsCount / 2 ? 1 : 0;
            var row = actualCarrots >= carrotsFunction.Length ? carrotsFunction.Length - 1 : actualCarrots;
            var k = carrotsFunction[row][distanceIndex];
            return actualCarrots * k / 50;
        }

        private int CalculateActualCarrots()
        {
            var carrots = haze.carrots;
            var cell = map.cells[haze.pos];
            if (cell is PositionCell posCell)
            {
                if (disp.activeHaze == playerIndex && posCell.OnRightPosition(disp.hazePos))
                    carrots += disp.hazePos * TurnChecker.CarrotsPerPosition;
                else
                {
                    if (posCell.OnRightPosition(disp.hazePos))
                        carrots += 10;
                }
            }
            else if (cell is SaladCell)
            {
                if (!haze.waits && disp.activeHaze == playerIndex)
                    carrots += disp.hazePos * TurnChecker.CarrotsPerSalad;
                else carrots += 15;
            }

            return carrots;
        }

        private int CalculateSaladScore()
        {
            var salad = haze.salad;
            var celLeft = saladCellsLeftFunction[haze.pos];
            if (map.cells[haze.pos] is SaladCell && haze.waits)
                salad = salad == 0 ? 0 : salad - 1;
            return saladFunction[celLeft][salad];
        }

        /// <summary>
        /// load carrots and salad functions from files
        /// in functions directory as 2D arrays
        /// </summary>
        private static void LoadFunctions()
        {
            saladCellsLeftFunction = LoadPlainArray(ExecutablePath.Combine("functions", "salad_cells_left.csv"));
            saladFunction = Load2DimArray(ExecutablePath.Combine("functions", "func_salad.csv"));
            carrotsFunction = Load2DimArray(ExecutablePath.Combine("functions", "func_carrots.csv"));
        }

        private static int[][] Load2DimArray(string path)
        {
            var rows = new List<int[]>();
            using (var sr = new StreamReader(path, Encoding.ASCII))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line)) continue;
                    var numbers = line.Split(';').Select(int.Parse).ToArray();
                    rows.Add(numbers);
                }
            }
            return rows.ToArray();
        }

        private static int[] LoadPlainArray(string path)
        {
            var rows = new List<int>();
            using (var sr = new StreamReader(path, Encoding.ASCII))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line)) continue;
                    rows.Add(int.Parse(line));
                }
            }
            return rows.ToArray();
        }
    }
}
