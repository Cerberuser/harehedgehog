﻿namespace HazeIgel.Model
{
    /// <summary>
    /// player, making move from this cell,
    /// receives 10*P carrots,
    ///  where P is player's position relative to other players,
    ///  if only this position equals to P
    /// </summary>
    public class PositionCell : BaseCell
    {
        /// <summary>
        /// position, P
        /// P = 1 also works for 5th and 6th player's position
        /// </summary>
        public readonly int position;

        public PositionCell(int pos)
        {
            position = pos;
        }

        public bool OnRightPosition(int hazePos)
        {
            return position == 1 ? (hazePos == position || hazePos == 5 || hazePos == 6) : hazePos == position;
        }
    }
}
