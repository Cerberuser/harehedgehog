﻿using System.Collections.Generic;

namespace HazeIgel.Model
{
    /// <summary>
    /// FieldMap extra description, used in calculations and checks
    /// does not depend on players' positions,
    /// thus can be calculated just once
    /// 
    /// TODO: incapsulate this class in FieldMap
    /// </summary>
    public class FieldMapDescriptor
    {
        /// <summary>
        /// nearest previous Igel's position for each cell index
        /// </summary>
        public readonly Dictionary<int, int> nearestIgelByPos = new Dictionary<int, int>();

        public FieldMapDescriptor(FieldMap map)
        {
            var igelPos = -1;
            for (var i = 0; i < map.fieldsCount - 1; i++)
            {
                nearestIgelByPos.Add(i, igelPos);
                if (map.cells[i] is IgelCell)
                    igelPos = i;
            }
        }
    }
}
