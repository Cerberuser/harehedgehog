﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HazeIgel.Model
{
    /// <summary>
    /// battlefield, a collection (array) of cells
    /// the object remains unchanged during the game process and
    /// while calculations and estimations are being performed
    /// </summary>
    public class FieldMap
    {
        /// <summary>
        /// used instead just FieldsCount => cells.Length
        /// because is called million times
        /// </summary>
        public readonly int fieldsCount;

        /// <summary>
        /// map itself as a collection of cells
        /// </summary>
        public readonly BaseCell[] cells;

        // singleton
        private static readonly Lazy<FieldMap> builder = new Lazy<FieldMap>(() => new FieldMap());

        // singleton
        public static FieldMap Instance => builder.Value;

        /// <summary>
        /// make a default (canonical) map
        /// </summary>
        private FieldMap()
        {
            fieldsCount = 65;
            cells = Enumerable.Range(0, fieldsCount).Select(i => (BaseCell)new CarrotCell()).ToArray();
            cells[0] = new StartCell();
            cells[fieldsCount - 1] = new FinishCell();
            new List<int> { 8, 11,15, 19, 24, 30, 37, 43, 50, 56 }.ForEach(i => cells[i] = new IgelCell());
            new List<int> { 16, 32, 48  }.ForEach(i => cells[i] = new PositionCell(1));
            new List<int> { 10, 17, 23, 29, 35, 41, 47, 53, 60 }.ForEach(i => cells[i] = new PositionCell(2));
            new List<int> { 4, 9, 12, 20, 28, 36, 44, 52 }.ForEach(i => cells[i] = new PositionCell(3));
            new List<int> { 9, 18, 27, 45, 54 }.ForEach(i => cells[i] = new PositionCell(4));
            new List<int> { 7, 22, 42, 57, 62 }.ForEach(i => cells[i] = new SaladCell());
            new List<int> { 3, 6, 14, 25, 31, 34, 39, 46, 51, 58, 61, 63 }.ForEach(i => cells[i] = new HaseCell());

            SetIgelsNextPositions();
        }

        /// <summary>
        /// calculate carrots required for moving N cells forward
        /// </summary>
        public static int GetCarrotsPerMove(int steps)
        {
            return (steps - 1) * (steps + 2) / 2 + 1;
        }

        /// <summary>
        /// get the number of cells a player can move forward
        /// providing he has "carrots" of carrots
        /// </summary>
        public static int GetMovesForCarrots(int carrots)
        {
            var root = (int) Math.Pow(8 * carrots + 1, 0.5);
            return (root - 1) / 2;
        }

        /// <summary>
        /// store the next Igel's position for each Igel cell,
        /// used in calculations and checks
        /// </summary>
        private void SetIgelsNextPositions()
        {
            for (var i = 0; i < cells.Length; i++)
                if (cells[i] is IgelCell cell)
                    cell.nextIgelPosition = i;
            var igels = cells.Where(c => c is IgelCell).Cast<IgelCell>().ToList();
            for (var i = 0; i < igels.Count - 1; i++)
                igels[i].nextIgelPosition = igels[i + 1].nextIgelPosition;
            igels.Last().nextIgelPosition = -1;
        }
    }
}
