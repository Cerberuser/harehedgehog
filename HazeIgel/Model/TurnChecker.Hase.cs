﻿using System;
using System.Linq;

namespace HazeIgel.Model
{
    /// <summary>
    /// special effects applied when a player hits the Hase cell
    /// </summary>
    public partial class TurnChecker
    {
        public enum HaseCellEffect
        {
            // apply random Hase effect, according to the game rules
            Default = 0,
            // apply no effect (for "explaining" a move)
            NoEffect,
            // special effects, used for solving Minimax tree
            AiResolverEffect,
            // effect chosen by its index, used for unit tests
            FixedEffect
        }

        /// <summary>
        /// predefined effect, used in solving Minimax tree
        /// </summary>
        public HaseCellEffect haseEffectKind = HaseCellEffect.Default;

        public int fixedHaseEffect;

        private void ApplyHaseEffect()
        {
            // do nothing when on a hase card... may be used when,
            // e.g., are providing extra info for possible steps
            if (haseEffectKind == HaseCellEffect.NoEffect)
                return;

            // used in AI calculations
            if (haseEffectKind == HaseCellEffect.AiResolverEffect)
            {
                // special case: when we're close to finish and have
                // enough carrots, let's suppose we'll be given extra carrots
                // (it is a BAD scenario for AI)
                if (newHaze.pos > 56)
                {
                    var carrotsRequiredToFinish = 
                        FieldMap.GetCarrotsPerMove(FieldMap.Instance.fieldsCount - newHaze.pos);
                    if (carrotsRequiredToFinish <= carrotsAfterMove ||
                        carrotsRequiredToFinish <= (55 - CarrotsMaxOnFinish))
                        newHaze.carrots = 65;
                }
                else                
                    newHaze.carrots = Math.Max(0, newHaze.carrots - 6);
                return;
            }

            var effectIndex = haseEffectKind == HaseCellEffect.FixedEffect
                ? fixedHaseEffect
                : random.Next(15);

            ApplyHaseEffectByNum(effectIndex);
        }

        private void ApplyHaseEffectByNum(int num)
        {
            if (num < 2)
                ApplySpareCarrot();
            else if (num < 4)
                ApplyWaitIfLeading();
            else if (num < 6)
                Apply65Carrots();
            else if (num < 8)
                ApplyCarrotsPerSalad();
            else if (num < 10)
                ApplyReturnHalfCarrots();
            else if (num < 12)
                ApplyFreeTurn();
            else if (num < 14)
                ApplyRevealCards();
            else
                ApplyTakeOneCarrot();
        }

        private void ApplyWaitIfLeading()
        {
            newDisp.haseEffects = "Wait if there are more than half of players behind you";

            var playersBehind = 0;
            for (var i = 0; i < disp.hazes.Length; i++)
            {
                if (i == disp.activeHaze) continue;
                if (newDisp.hazes[i].pos < newHaze.pos) playersBehind++;
            }
            if (playersBehind < Math.Ceiling(disp.hazes.Length / 2M)) return;
            newHaze.waits = true;
        }

        private void Apply65Carrots()
        {
            newDisp.haseEffects = "Make your carrots- count 65";
            newHaze.carrots = 65;
        }

        private void ApplyReturnHalfCarrots()
        {
            newDisp.haseEffects = "Return half of your carrots to bank";
            newHaze.carrots /= 2;
        }

        private void ApplyFreeTurn()
        {
            newDisp.haseEffects = "Get carrots back for your move";
            var delta = FieldMap.GetCarrotsPerMove(cmd);
            var newCarrots = newHaze.carrots + delta;
            newHaze.carrots += Math.Max(newCarrots, haze.carrots);
        }

        private void ApplyRevealCards()
        {
            newDisp.haseEffects = $"Player {disp.activeHaze + 1} has {newHaze.carrots} carrots";
        }

        private void ApplyCarrotsPerSalad()
        {
            newDisp.haseEffects = "Take 10 carrots per each of your salad card. Wait if you are short of salad";
            if (newHaze.salad == 0)
            {
                newHaze.waits = true;
                return;
            }
            newHaze.carrots += newHaze.salad * 10;
        }

        private void ApplyTakeOneCarrot()
        {
            newDisp.haseEffects = "Take one carrot from each player";

            for (var i = 0; i < newDisp.hazes.Length; i++)
            {
                if (i == disp.activeHaze) continue;
                if (newDisp.hazes[i].carrots == 0) continue;
                if (newDisp.hazes[i].carrots > 0)
                {
                    newDisp.hazes[i].carrots--;
                    newHaze.carrots++;
                }
            }
        }

        private void ApplySpareCarrot()
        {
            newDisp.haseEffects = "Give 10 carrots each player behind you";
            
            var carrotsSum = 0;
            var carrotsPerHaze = new int[disp.hazes.Length];
            for (var i = 0; i < disp.hazes.Length; i++)
            {
                if (i == disp.activeHaze) continue;
                if (disp.hazes[i].pos >= newHaze.pos) continue;

                // does haze want to take this carrot?
                var carrotsPerFinish = map.fieldsCount - 1 - disp.hazes[i].pos;
                carrotsPerFinish = FieldMap.GetCarrotsPerMove(carrotsPerFinish);
                if (disp.hazes[i].carrots > carrotsPerFinish + 10)
                    continue;
                carrotsPerHaze[i] = 10;
                carrotsSum += 10;
            }
            if (carrotsSum > newHaze.carrots)
            {
                carrotsPerHaze = carrotsSum <= newHaze.carrots / 2 ?
                    carrotsPerHaze.Select(c => c / 2).ToArray() :
                    carrotsPerHaze.Select(c => c == 0 ? 0 : 1).ToArray();
            }

            carrotsSum = 0;
            for (var i = 0; i < disp.hazes.Length; i++)
            {
                if (i == disp.activeHaze) continue;
                if (carrotsSum + carrotsPerHaze[i] > newHaze.carrots)
                    break;
                newDisp.hazes[i].carrots += carrotsPerHaze[i];
                carrotsSum += carrotsPerHaze[i];
            }
            newHaze.carrots -= carrotsSum;
        }
    }
}
