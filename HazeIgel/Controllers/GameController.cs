﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HazeIgel.Model;
using HazeIgel.Model.Game;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HazeIgel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        [HttpGet]
        [Route("~/api/[controller]")]
        public ActionResult<string> Help()
        {
            return new ActionResult<string>(GetHelp());
        }
        
        // POST api/game
        [HttpPost]
        [Route("~/api/[controller]/turn")]
        public IActionResult Turn([FromBody] string json)
        {
            var resp = Model.Game.GameController.ProcessCommand(json);
            var respJson = JsonConvert.SerializeObject(resp);
            return Ok(respJson);
        }

        private static string GetHelp()
        {
            var samples = new List<GameCommand>
            {
                new GameCommandNewGame
                {
                    aiPlayers = 1,
                    humanPlayers = 2
                },
                new GameCommandPlayerTurn
                {
                    command = 3,
                    disp = new Disposition
                    {
                        hazes = new []
                        {
                            new Haze(0)
                            {
                                pos = 11,
                                carrots = 23,
                                salad = 2,
                                waits = false
                            },
                            new Haze(1)
                            {
                                pos = 26,
                                carrots = 1,
                                salad = 0,
                                waits = true
                            }
                        },
                        activeHaze = 1
                    }
                }
            };
            var jSamples = samples.Select(s => JsonConvert.SerializeObject(s, Formatting.Indented)).ToList();

            var sb = new StringBuilder("You should make a post request on ~/api/Game/turn. Samples are:");
            sb.AppendLine();
            foreach (var s in jSamples)
            {
                sb.AppendLine();
                sb.AppendLine(s);
            }
            return sb.ToString();
        }
    }
}
