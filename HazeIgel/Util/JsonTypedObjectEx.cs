﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace HazeIgel.Util
{
    /// <summary>
    /// stores class information in JSON
    /// can be deserialized into derived class's object
    /// </summary>
    public abstract class JsonTypedObjectEx
    {
        private static readonly Regex _commandNameRegex = new Regex("(?<=\"ClassName\":\\s*\")[a-z,A-Z,_,0-9]+");

        [Required]
        public string ClassName { get; protected set; }

        protected JsonTypedObjectEx()
        {
            ClassName = GetType().Name;
            var attr = GetType().GetCustomAttributes(typeof(JsonClassNameAttribute), true).FirstOrDefault() as
                        JsonClassNameAttribute;
            if (attr != null) ClassName = attr.Name;
        }

        public string Serialize()
        {
            return Serialize(null);
        }

        public string Serialize(JsonSerializerSettings settings)
        {
            return settings == null ? JsonConvert.SerializeObject(this) : JsonConvert.SerializeObject(this, settings);
        }

        protected static JsonTypedObjectEx Deserialize(string commandJson, Dictionary<string, Type> commandByName)
        {
            return Deserialize(commandJson, commandByName, null);
        }

        protected static JsonTypedObjectEx Deserialize(string commandJson, Dictionary<string, Type> commandByName,
            JsonSerializerSettings settings)
        {
            if (string.IsNullOrEmpty(commandJson))
                return null;

            var nameMatch = _commandNameRegex.Match(commandJson);
            if (string.IsNullOrEmpty(nameMatch.Value))
                return null;

            var cmdName = nameMatch.Value;
            if (!commandByName.TryGetValue(cmdName, out Type commandType))
            {
                //Error($"Unknown request type " + cmdName);
                return null; // unknown command
            }

            try
            {
                var cmd = (JsonTypedObjectEx)(settings == null
                        ? JsonConvert.DeserializeObject(commandJson, commandType)
                        : JsonConvert.DeserializeObject(commandJson, commandType, settings));
                return cmd;
            }
            catch (Exception ex)
            {
                // Error($"Error parsing \"{commandJson}\": {ex}");
                return null;
            }
        }

        protected static Dictionary<string, Type> ReadTypeByName(Type inheretedType)
        {
            var typeByName = new Dictionary<string, Type>();
            var asm = inheretedType.Assembly;
            foreach (var type in asm.GetTypes())
            {
                if (!inheretedType.IsAssignableFrom(type)) continue;

                var typeName = type.Name;
                var nameAttr = type.GetCustomAttributes(typeof(JsonClassNameAttribute), true).FirstOrDefault() as
                        JsonClassNameAttribute;
                if (nameAttr != null)
                    typeName = nameAttr.Name;
                typeByName.Add(typeName, type);
            }
            return typeByName;
        }
    }

}
