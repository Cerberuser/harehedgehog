﻿using System.Collections.Generic;
using System.Linq;

namespace HazeIgel.Util
{
    public static class CollectionExtension
    {
        public static string FormatMinMaxAvg(this List<int> items)
        {
            if (items == null || items.Count == 0) return "-";
            return $"{items.Count} values, from {items.Min()} to {items.Max()}, average: {items.Average()}";
        }

        public static string FormatMinMaxAvg(this List<float> items)
        {
            if (items == null || items.Count == 0) return "-";
            return $"{items.Count} values, from {items.Min()} to {items.Max()}, average: {items.Average()}";
        }

        public static string FormatMinMaxAvg(this List<double> items)
        {
            if (items == null || items.Count == 0) return "-";
            return $"{items.Count} values, from {items.Min()} to {items.Max()}, average: {items.Average()}";
        }

        public static string FormatMinMaxAvg(this List<decimal> items)
        {
            if (items == null || items.Count == 0) return "-";
            return $"{items.Count} values, from {items.Min()} to {items.Max()}, average: {items.Average()}";
        }
    }
}
